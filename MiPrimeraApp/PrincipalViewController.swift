//
//  PrincipalViewController.swift
//  MiPrimeraApp
//
//  Created by Kevin Belter on 11/8/16.
//  Copyright © 2016 Kevin Belter. All rights reserved.
//

import UIKit

class PrincipalViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    var valorAObtener = 15 {
        didSet {
            print(valorAObtener)
        }
    }
    
    @IBAction func tocoMultipleManualmente2() {
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //Forma 1:
        /*
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "MultipleToBtcViewController") as! MultipleToBtcViewController
        vc.valorQueDeseoObtenerDeOtroViewController = 15
        */
        
        let vc = MultipleToBtcViewController.newInstance(quienMeAbrio: self, valor: 15, storyboard: self.storyboard!)
        
        //self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func tocoAbrirManualmente() {
        performSegue(withIdentifier: "openMultipleToBtc", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "openMultipleToBtc" {
            let vc = segue.destination as! MultipleToBtcViewController
            vc.valorQueDeseoObtenerDeOtroViewController = 16
        }
    }
}
