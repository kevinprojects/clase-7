//
//  MultipleToBtcViewController.swift
//  MiPrimeraApp
//
//  Created by Kevin Belter on 11/22/16.
//  Copyright © 2016 Kevin Belter. All rights reserved.
//

import UIKit

class MultipleToBtcViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var monedasPickerView: UIPickerView!
    @IBOutlet weak var txtValor: UITextField!
    @IBOutlet weak var lblResultado: UILabel!
    
    let lastRowForPickerView = "lastRowForPickerView"
    
    var valorQueDeseoObtenerDeOtroViewController: Int!
    weak var quienMeAbrio: PrincipalViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monedasPickerView.dataSource = self
        monedasPickerView.delegate = self
        
        guard let row = UserDefaults.standard.value(forKey: lastRowForPickerView) as? Int else { return }
        
        monedasPickerView.selectRow(row, inComponent: 0, animated: true)
    }
    
    @IBAction func tocoCerrarModal() {
        quienMeAbrio.valorAObtener = 30
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tocoConvertir() {
        
        guard let valorString = self.txtValor.text, let valor = Double(valorString) else {
            //Presentar alerta: Por favor ingrese un numero valido.
            let alert = UIAlertController(title: "Error", message: "Por favor ingrese un numero valido", preferredStyle: UIAlertControllerStyle.alert)
            
            //Forma mas larga.
            /*
            let accionAceptar = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { _ in
                alert.dismiss(animated: true, completion: nil)
            })
            */
            
            //Forma intermedia.
            let accionAceptar = UIAlertAction(title: "OK", style: .default) { alertAction in
                alert.dismiss(animated: true, completion: nil)
            }
            
            //Forma mas facil.
            //let accionAceptar = UIAlertAction(title: "OK", style: .cancel, handler: nil)

            alert.addAction(accionAceptar)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        
        let rowSeleccionada = monedasPickerView.selectedRow(inComponent: 0)
        let divisa = divisas[rowSeleccionada]
        
        let cotizador = Cotizador()
        
        activityIndicator.startAnimating()
        cotizador.obtenerValorBTC(divisa: divisa) { valorBTCOpcional in
            
            if let valorBTC = valorBTCOpcional {
                let resultado = valor / valorBTC
                self.lblResultado.text = "\(resultado)"
                self.activityIndicator.stopAnimating()
            }
        }
        txtValor.resignFirstResponder()
    }
    
    fileprivate var divisas = ["ARS", "USD", "MXN", "EUR", "KAM", "POM"]
}

extension MultipleToBtcViewController {
    static func newInstance(quienMeAbrio: PrincipalViewController, valor: Int, storyboard: UIStoryboard) -> MultipleToBtcViewController {
        let vc = storyboard.instantiateViewController(withIdentifier: "MultipleToBtcViewController") as! MultipleToBtcViewController
        vc.valorQueDeseoObtenerDeOtroViewController = valor
        vc.quienMeAbrio = quienMeAbrio
        return vc
    }
}

extension MultipleToBtcViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return divisas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return divisas[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        UserDefaults.standard.setValue(row, forKey: lastRowForPickerView)
        UserDefaults.standard.synchronize()
    }
}
